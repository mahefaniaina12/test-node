const request = require('supertest');
const app = require('../src/app');

describe('prize test', () => {
    it('get all nobel prize', done => {
          request(app).get('/prize')
            .expect(200, done)
    });
    it('get nobel prize by year and category', done => {
          request(app).get('/prize/2019?category=medicine')
            .expect(200, done)
    });
    it('default 404 not found', done => {
          request(app).get('/dumbendpoint')
            .expect(404, done)
    });
})