const express = require ('express');
const app = express();
const bodyParser = require('body-parser');
const prize = require('./prize');

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use('/prize', prize)

app.use((req, res) => {
    res.send(404);
});

app.listen(3000)

module.exports = app;