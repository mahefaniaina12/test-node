const express = require('express');
const app = express();
const axios = require('axios');

const URL = 'http://api.nobelprize.org/v1/prize.json';

// Return all the nobel prize winner
app.get('/', (req, res) => {
    axios.get(URL)
        .then(response => {
            res.send(response.data.prizes);
        })
        .catch(err => {
            throw err;
        })
})

// return nobel prize by year and category 
app.get('/:year', (req, res) => {
    let prizeList = [];
    let { year } = req.params;
    axios.get(URL)
        .then(response => {
            let filteredList = []
            prizeList = response.data.prizes;
            filteredList = prizeList.filter(prize => prize.year === year);
            if (req.query && req.query.category) {
                filteredList = filteredList.filter(prize => prize.category === req.query.category)
            }
            res.send(filteredList);
        })
        .catch(err => {
            throw err;
        })
})

module.exports = app; 